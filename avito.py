from connect import Connect
import requests
from bs4 import BeautifulSoup
import re
from _datetime import datetime
import time
import random


class solution2:
    def __init__(self):
        conn = Connect("rand")
        result_html = self.get_html(conn)
        list = self.get_links(result_html, conn)
        self.information = self.get_inf(list, conn)

    def get_html(self, conn):
        # получение html
        response = requests.get(
            "https://www.avito.ru/perm/kvartiry/sdam/na_dlitelnyy_srok/1-komnatnye",
            headers= conn.user_agent("rand")
        )
        return response.text

    def get_links(self, res, conn):
        # получение ссылок на объявления
        list_of_links = []
        soup = BeautifulSoup(res, "html.parser")
        links = soup.find_all('a', class_="item-description-title-link")
        for i in links:
            list_of_links.append(i.get('href'))
        page_number = soup.find_all('a', class_="pagination-page")
        page_count = 0
        for i in page_number:
            page_count = max(re.findall('\d+',  i.get('href')))
        for i in range(1, int(page_count)):
            time.sleep(random.randint(1, 4))
            zapros = requests.get(
                'https://www.avito.ru/perm/kvartiry/sdam/na_dlitelnyy_srok/1-komnatnye?p={0}'.format(str(i)),
            headers = conn.user_agent("rand"))
            so = BeautifulSoup(zapros.text, "html.parser")
            links = so.find_all('a', class_="item-description-title-link")
            for j in links:
                list_of_links.append(j.get('href'))
        return list_of_links

    def get_inf(self, list, conn):
        #получение информации со страницы
        list_of_flats = []
        for i in list:
            time.sleep(random.randint(1, 3))
            _zapros = requests.get('https://www.avito.ru{0}'.format(str(i)),
                                  headers = conn.user_agent("rand")
                                  )
            next = BeautifulSoup(_zapros.text, "html.parser")
            try:
                _destination = next.find('span', class_="item-map-address")
                _type = next.find_all('li', class_='item-params-list-item')
                _price = next.find('span', class_="price-value-string js-price-value-string")
                _flat_price = re.findall('\d+', _price.get_text().replace(' ', ''))
                _flat_destination = _destination.get_text().replace("\n", "")
                properties = []
                for i in _type:
                    properties.append(i.get_text().replace("\n", "").split(":"))
                flat_description = {
                    'price': _flat_price[0],
                    'floor_current': properties[1][1],
                    'total_floor': properties[2][1],
                    'type_of_house': properties[3][1],
                    'adress': _flat_destination
                }
                list_of_flats.append(flat_description)
            except AttributeError:
                time.sleep(1)
        return list_of_flats


sol_avito = solution2()
for i in sol_avito.information:
    print(i['price'] +
          i['floor_current']+
          i['total_floor'] +
          i['type_of_house'] +
          i['adress'])